form_txt = {};

form_txt.to_txt = function(form) {
	return '' +
		'Votre nom\n' +
		'Nom: ' + form.get('person').last + '\n' +
		'Prénom: ' + form.get('person').first + '\n' +
		'Nom de jeune fille: ' + form.get('person').maiden + '\n' +
		'\n' +
		'Votre naissance\n' +
		'Date de naissance: ' + form.get('person').birthdate + '\n' +
		'Lieu de naissance: ' + form.get('person').birthplace + '\n' +
		'\n' +
		'Votre situation personnelle\n' +
		'État civil: ' + form.get('person').civil + '\n' +
		'Nationalité: ' + form.get('person').nationality + '\n' +
		'Religion: ' + form.get('person').religion + '\n' +
		'\n' +
		'Votre adresse légale\n' +
		'Rue, N°: ' + form.get('address').name + '\n' +
		'Code postal: ' + form.get('address').postal + '\n' +
		'Ville: ' + form.get('address').city + '\n' +
		'Pays: ' + form.get('address').country + '\n' +
		'\n' +
		'Vous contacter\n' +
		'Téléphone: ' + form.get('contact').phone + '\n' +
		'Email: ' + form.get('contact').email + '\n' +
		'\n' +
		'Votre profession\n' +
		'Profession: ' + form.get('job').profession + '\n' +
		'Employeur: ' + form.get('job').employer + '\n' +
		'Téléphone professionnel: ' + form.get('job').phone + '\n' +
		'Adresse courrier: ' + form.get('job').address + '\n' +
		'Canton / Commune: ' + form.get('job').region + '\n' +
		'\n' +
		'Votre assurance\n' +
		'Assurance maladie: ' + form.get('insurance').health + '\n' +
		'Assurance accident: ' + form.get('insurance').work + '\n' +
		'Couverture d\'assurance: ' + form.get('insurance').cover + '\n' +
		'\n' +
		'En cas d\'urgence\n' +
		'Téléphone de contact en cas d\'urgence: ' + form.get('emergency').number + '\n' +
		'\n' +
		'Votre consultation médicale\n' +
		'Antécédents médicaux: ' + form.get('medical').medicalHistory + '\n' +
		'Antécédents chirurgicaux: ' + form.get('medical').surgeryHistory + '\n' +
		'Maladies actives: ' + form.get('medical').diseases + '\n' +
		'Allergies: ' + form.get('medical').allergies + '\n' +
		'Médicaments actuels: ' + form.get('medical').drugs + '\n' +
		'Motif de consultation: ' + form.get('medical').consultancyReason + '\n' +
		'Besoin d\'un interpréteur: ' + form.get('medical').interpreterNeeded + '\n' +
		'Langues parlées: ' + form.get('medical').spokenLanguages + '\n' +
		'Urgence ressentie: ' + form.get('medical').subjectiveEmergency + '\n' +
		'Autre: ' + form.get('medical').other
}