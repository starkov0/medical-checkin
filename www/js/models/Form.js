/* Modele correspondant a des Users au sens GMP */
window.Form = Backbone.Model.extend({
	urlRoot: window.RestUrlPrefix + "form",

	encrypt_attributes: function(password) {
		for(attribute in this.attributes) {
			for(element in this.attributes[attribute]) {
				var str = this.attributes[attribute][element];

				if (str == '') {
					continue;
				}

				str = crypto.encrypt(password, str);
				this.attributes[attribute][element] = str;
			}
		}
	},

	decrypt_attributes: function(password) {
		console.log("Will decrypt with:"+password);
		//Will create empty model just for attribute names
		var virgin_model = new Form();

		var reconstituted_pwd = crypto.reconstitute_pwd(password);

		for(attribute in virgin_model.attributes) {
			for(element in virgin_model.attributes[attribute]) {
				var str = this.attributes[attribute][element];

				if(str == '')
					continue;

				//CAN throw an exception
				str = crypto.decrypt(reconstituted_pwd, str);

				this.attributes[attribute][element] = str;
			}
		}
	},

	//Attributes; sending to the server
	defaults: {
		person: {
			last: '',
			first: '',
			maiden: '',
			birthdate: '',
			birthplace: '',
			civil: '',
			nationality: '',
			religion: ''
		},
		address: {
			name: '',
			postal: '',
			city: '',
			country: ''
		},
		contact: {
			phone: '',
			email: ''
		},
		job: {
			profession: '',
			employer: '',
			phone: '',
			address: '',
			region: ''
		},
		insurance: {
			health: '',
			work: '',
			cover: ''
		},
		emergency: {
			number: ''
		},
		medical: {
			medicalHistory: '',
			surgeryHistory: '',
			diseases: '',
			allergies: '',
			drugs: '',
			consultancyReason: '',
			interpreterNeeded: '',
			spokenLanguages: '',
			subjectiveEmergency: '',
			other: ''
		}
	},

	fetch: function(options) {
		//Force non async fetching
		options.async = false;
		Backbone.Model.prototype.fetch.call(this, options);
	}
});

