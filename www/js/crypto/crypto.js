/* encryption.js
 * A little all-in-one lib which manages generating and re-creating a key.
 * Copyright Frank Villaro-Dixon, BSD Simplified, 2014
 */

crypto.key_size = 128; //bits

crypto.word_separator = ' ';

crypto.take_closer_word = function(input_word) {
	var closer_word_dist = 999;
	var closer_word = 'snhshn';

	//Does the word exist ?
	for(var i = 0; i < crypto.words.length; i++) {
		if(input_word == crypto.words[i])
			return input_word;
	}

	//There was a transmission error. Take the closest one
	for(var i = 0; i < crypto.words.length; i++) {
		var d = new Levenshtein(input_word, crypto.words[i]);
		//console.log('Btw '+input_word+' and '+crypto.words[i]+' : '+d.distance);
		if(d < closer_word_dist) {
			closer_word_dist = d.distance;
			closer_word = crypto.words[i]
		}
	}
	return closer_word;
}


//Generate a password consisting of words
crypto.generate_pwd = function() {
	//Calculate the words we would need to have a key size of crypto.key_size bits
	var words_we_have = crypto.words.length;
	var words_needed = Math.round( //log_{wwh}(2¹²⁸)
			Math.log(Math.pow(2, crypto.key_size)) / //2^key_size possibilities
			Math.log(words_we_have)) //Our words

	console.log("We need "+words_needed+" words"+
			"("+words_we_have+"^"+words_needed+" = 2^"+crypto.key_size+")");

	var password = "";
	for(var i = 0; i < words_needed; i++) {
		var word_choosen = Math.floor(Math.random() * words_we_have);
		password += crypto.words[word_choosen];
		if(i < words_needed-1)
			password += crypto.word_separator;
	}

	return password;
}

//If the user makes a mistake (eg: mastake instead of mistake), correct-it
crypto.reconstitute_pwd = function(input_pw) {
	input_pw = input_pw.trim().toLowerCase();

	var words = input_pw.split(crypto.word_separator);
	var password = '';
	for(var i = 0; i < words.length; i++) {
		password += crypto.take_closer_word(words[i]);
		if(i < words.length - 1)
			password += crypto.word_separator;
	}

	return password;
}

crypto.encrypt = function(password, str) {
	return GibberishAES.enc(str, password);
}

crypto.decrypt = function(password, str) {
	return GibberishAES.dec(str, password);
}



