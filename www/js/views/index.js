window.IndexView = Backbone.View.extend({

	initialize: function() {
		this.template = _.template(tpl.get('index'));
		window.form = new Form()
		window.password = crypto.generate_pwd()
	},

	events: {
		"click #submit": "submit"
	},

	submit: function () {
		//Our forms have the same structure as our model's attributes
		for(attribute in window.form.attributes) {
			for(element in window.form.attributes[attribute]) {
				var val = $('#'+attribute+'_'+element).val();

				console.log(val instanceof Date)

				//If we have a radio button
				if(val == undefined) {
					val = $('[name='+attribute+'_'+element+']:checked').val();
					if(val == undefined) {
						val = '';
					}
				}

				window.form.attributes[attribute][element] = val;
			}
		}

		window.form.encrypt_attributes(window.password)
		window.form.save(null, {
			success: function() {
				app.navigate('confirmation', {trigger: true})
			}
		});
	},

	render: function() {
		$(this.el).html(this.template({
			form: window.form
		}));
		return this;
	}
});

