window.ConfirmationView = Backbone.View.extend({

	initialize: function() {
		this.template = _.template(tpl.get('confirmation'));
	},

	render: function() {
		$(this.el).html(this.template({
			form: window.form,
			password: window.password
		}));

		if (form != null) {
			//This is UGLY ! fixme
			new QRCode(this.el.lastElementChild,
				'{"id": "'+window.form.get('id')+'"; ' +
				'"password":"'+ window.password+'"}');
		}

		return this;
	}
});