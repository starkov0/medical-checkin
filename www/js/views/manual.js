window.ManualView = Backbone.View.extend({

	initialize: function() {
		this.template = _.template(tpl.get('manual'));
	},

	render: function() {
		$(this.el).html(this.template({}));

		return this;
	}
});

