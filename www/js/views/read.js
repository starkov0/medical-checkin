window.ReadView = Backbone.View.extend({

	initialize: function() {
		this.template = _.template(tpl.get('read'));
		this.form_template = _.template(tpl.get('form_view_html'));
		this.txt_template = _.template(tpl.get('form_view_txt'));
	},

	events: {
		"click #read_submit": "read_submit",
		"click #download_txt_submit": "download_txt_submit",
		"click #download_pdf_submit": "download_pdf_submit"
	},

	get_and_decrypt_form: function() {
		var that = this;

		//Only to be sure
		$("#bad_id").hide();
		$("#bad_pwd").hide();

		var id = $('#form_id').val().trim().toLowerCase();
		var pwd = $('#form_pwd').val();

		if(id.length < 2) {
			$("#bad_id").show();
			return 0;
		}

		window.form = new Form({id: id});
		window.password = pwd;

		try {
			//This is synchronous
			window.form.fetch({
				error: function() {
					throw "Bad id";
				}
			});
			window.form.decrypt_attributes(window.password);
		} catch(e) {
			//Bad id
			if(e == "Bad id") {
				$("#bad_id").show();
			}
			//Decryption error
			else {
				$("#bad_pwd").show();
			}
			return 0;
		}
		return 1;
	},

	read_submit: function () {
		if(this.get_and_decrypt_form())
			this.showForm();
	},

	download_txt_submit: function() {
		if(!this.get_and_decrypt_form())
			return;

		var data = this.txt_template({form: window.form});
		var fileName = "medical-checkin.txt";

		var blob = new Blob([data], {encoding:"UTF-8",type:"text/plain;charset=UTF-8"});
		saveAs(blob, fileName);
	},

	download_pdf_submit: function() {
		if(!this.get_and_decrypt_form())
			return;

		var data = form_txt.to_txt(window.form)
		var fileName = "medical-checkin.pdf";

		var docDefinition = { content: data }
		pdfMake.createPdf(docDefinition).download(fileName);
	},


	render: function() {
		$(this.el).html(this.template({
			form: window.form
		}));
		return this;
	},

	showForm: function() {
		$("#read_results").html(this.form_template({
			form: window.form
		}));
	}
});

