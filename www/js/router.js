/* ROUTEUR DE L'APPLICATION */
var AppRouter = Backbone.Router.extend({

	initialize: function() {
		//Initialize the publish/subscribe bus
		window.pubSubBus = _.extend({}, Backbone.Events);

		//the user form
		window.form = null
		window.password = null;
	},

	routes: {
		'' : 'index',
		'confirmation': 'confirmation',
		'read': 'read',
		'about' : 'about',
		'manual' : 'manual'
	},

	index: function() {
		app.showViewContent(new IndexView());
	},
	confirmation: function() {
		app.showViewContent(new ConfirmationView());
	},

	read: function() {
		app.showViewContent(new ReadView());
	},

	manual: function() {
		app.showViewContent(new ManualView());
	},
	about: function() {
		app.showViewContent(new AboutView());
	},

	showViewContent: function(view) {
		if (this.currentViewContent)
			this.currentViewContent.close(); //on detatch l'ancienne view

		$('#content').html(view.render().el); //Affiche la nouvelle
		this.currentViewContent = view;
		return view;
	},

});

